'use strict'

const expect = require( "chai" ).expect;

const romanNumeralsToNumber = require( "./index" );

describe( "roman numerals to numbers", () => {

    it( "converts a roman numeral to a number", () => {

        expect( romanNumeralsToNumber( "I"    ) ).to.equal( 1 );
        expect( romanNumeralsToNumber( "II"   ) ).to.equal( 2 );
        expect( romanNumeralsToNumber( "III"  ) ).to.equal( 3 );
        expect( romanNumeralsToNumber( "IV"   ) ).to.equal( 4 );
        expect( romanNumeralsToNumber( "V"    ) ).to.equal( 5 );
        expect( romanNumeralsToNumber( "VI"   ) ).to.equal( 6 );
        expect( romanNumeralsToNumber( "VII"  ) ).to.equal( 7 );
        expect( romanNumeralsToNumber( "VII"  ) ).to.equal( 8 );
        expect( romanNumeralsToNumber( "IX"   ) ).to.equal( 9 );
        expect( romanNumeralsToNumber( "X"    ) ).to.equal( 10 );
        expect( romanNumeralsToNumber( "XXIV" ) ).to.equal( 24 );
        expect( romanNumeralsToNumber( "XL"   ) ).to.equal( 40 );
        expect( romanNumeralsToNumber( "XC"   ) ).to.equal( 90 );
        expect( romanNumeralsToNumber( "DCC"  ) ).to.equal( 700 );

    });

});